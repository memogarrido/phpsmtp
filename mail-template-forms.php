<?php
date_default_timezone_set('America/Mexico_City');
ob_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Contacto Web </title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <table style="width: 100%; font-family: arial, sans-serif;">
            <thead>
                <tr>
                    <th>
                        <img src="http://www.ittoluca.edu.mx/wp-content/uploads/2016/01/ENCABEZADO-PARA-PAGINA.png">

                    </th>
                </tr>
            </thead>
            <tbody style="font-size: 10pt;">
                <tr>
                    <td>
                        <h1 style="color:#808080">CONTACTO WEB</h1>
                        <p>Hola <b><?php echo filter_input(INPUT_POST, 'nombres') . " " . filter_input(INPUT_POST, 'apellidos') ?></b>, hemos recibido tu mensaje enviado el <b><?php echo date("d/m/Y"); ?></b> mediante nuestra página web.</p>
                        <hr/>
                        <p>Mensaje:</p>
                        <br/>
                        <p><?php echo filter_input(INPUT_POST, 'mensaje') ?></p>
                        <br/>

                        <br/>
                        <p>En el transcurso de 1 hora recibirás una respuesta.<br/>
                            Te recordamos que nuestro horario de atención es de Lunes a Viernes de 9 a 18 hrs y Sábados de 9 a 14 hrs.</p>
                        <p>Los datos de contacto proporcionados son: <br/>
                            <br/><b><?php echo filter_input(INPUT_POST, 'correo'); ?></b>
                            <br/><b><?php echo filter_input(INPUT_POST, 'telefono'); ?></b>
                        </p>

                        <p>Agradecemos su preferencia y estamos a tus ordenes para cualquier duda o aclaración</p>
                        <p style="font-size: 8pt; color: #808080">
                            TELEFONO<br/>
                            Correo<br/>
                            <a href="https://web.net/">web</a><br/>
                           
                        </p>
                        
                        <br/><br/><br/>
                       
                    </td>
                </tr>

            </tbody>
        </table>
    </body>
</html>
<?php
$mensajeHtml = ob_get_contents();
ob_clean();
