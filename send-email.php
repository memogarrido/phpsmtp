<?php

include("PHPMailer/class.phpmailer.php");
include("PHPMailer/class.smtp.php");
if ((empty(filter_input(INPUT_POST, 'nombres')) == FALSE) && (empty(filter_input(INPUT_POST, 'correo')) == FALSE)) {//si nombre y correo tienen contenido
    $mail = new PHPMailer();
    //$mail->SMTPDebug = true;
    //$mail->Debugoutput = 'echo';
    //$mail->IsSMTP();  // Usar SMTP para enviar un correo mediante Gmail
    $mail->Mailer = "smtp";
    $mail->Host = "ssl://smtp.gmail.com"; //Host - Parametro de configuración de Gmail
    $mail->Port = 465; //Puerto - Parametro de configuración de Gmail
    $mail->SMTPAuth = true; // Autentificación - Parametro de configuración de Gmail
    $mail->Username = "correo@gmail.com"; //Usuario - Parametro de configuración de Gmail
    $mail->From = "correo@gmail.com"; //Usuario - Parametro de configuración de Gmail
    $mail->FromName = "Nombre de quien envia";
    $mail->Password = "PASSWORD"; // Contraseña - Parametro de configuración de Gmail
    $mail->IsHTML(true);
    $mail->Subject = "ASUNTO DEL CORREO";
    $mail->CharSet = 'UTF-8';
    include './mail-template-forms.php'; //Plantilla con diseño del correo
    $mail->msgHTML($mensajeHtml);
    $address = "atencion@prosic.net";
    $mail->AddAddress($address, "Atención PROSIC");
    $address2 = filter_input(INPUT_POST, 'correo');
    $mail->addAddress($address2, filter_input(INPUT_POST, 'nombres') . " " . filter_input(INPUT_POST, 'apellidos'));

    if (!$mail->Send()) {
        header("location:/" . filter_input(INPUT_POST, 'seccion') . "/gracias.php?result=-1" . $mail->ErrorInfo . $i); //advertencia
    } else {
        header("location:/" . filter_input(INPUT_POST, 'seccion') . "/gracias.php?result=1");
    }
} else {
    header("location:/" . filter_input(INPUT_POST, 'seccion') . "/?result=0");
}